from WordsSet import WrdsSetInstance
from simple_term_menu import TerminalMenu

class Command: 
  def __init__(self, command, action):
    self.command = command
    self.action = action

def exitVocabulary():
  WrdsSetInstance.writeWords()
  exit()

def files():
  from os import walk
  WrdsSetInstance.writeWords()
  path = './words'
  filenames = next(walk(path), (None, None, []))[2]  # [] if no file
  terminal_menu = TerminalMenu(filenames)
  choice_index = terminal_menu.show()
  WrdsSetInstance.loadWordsFile('./words/' + filenames[choice_index])

def printWords():
  WrdsSetInstance.printWords()

def restartStatistic():
  WrdsSetInstance.restartWordsSetStatistic()

def showHelp():
  print('e - save and exit')
  print('f - files')
  print('p - print words')
  print('r - restarts current words set statistics')
  print('h - help')

commands = [
  Command("e", exitVocabulary),
  Command("f", files),
  Command("p", printWords),
  Command("r", restartStatistic),
  Command("h", showHelp),
]
