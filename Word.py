from re import X
import json
import types

class Word:
  def __init__(self, word):
    self.DE = word['DE']
    self.CZ = word['CZ']
    self.attempts = 1
    self.success = word['success']
    self.fail = word['fail']
    self.successRatio = word['successRatio']

  def simplify(self):
    simpleWord = types.SimpleNamespace()
    simpleWord.DE = self.DE 
    simpleWord.CZ = self.CZ
    simpleWord.success = self.success 
    simpleWord.fail = self.fail 
    simpleWord.successRatio = self.successRatio
    return simpleWord

  def updateRatio(self):
    if self.fail == 0:
      self.successRatio = 100
    else:
      self.successRatio = self.success / (self.fail + self.success) * 100
    
  # recalculate all counters, attempts and ratios on success/fail
  def updateSuccessCounters(self, success):
    if success:
      self.attempts = self.attempts - 1
      self.success = self.success + 1
    else:
      self.attempts = self.attempts + 2
      self.fail = self.fail + 1
    self.updateRatio()

  def restartAttempts(self):
    self.attempts = 1
  
  def restartWordStatistic(self):
    self.attempts = 1
    self.success = 0
    self.fail = 0
    self.successRatio = 0

  def repeatToRemember(self):
      repeat = 0
      while True:
        correctInput = input('correct yourself' + ': ')
        if correctInput == self.DE:
          print('\u001b[32m \u2713 \033[0m ')
          if repeat >= 3:
            break
          else:
            repeat = repeat + 1
        else:
          print('\u001b[31m \u2717 \033[0m')


  def checkWord(self, wDE):
    if self.DE == wDE:
      self.updateSuccessCounters(True)
      print('\u001b[32m \u2713 \033[0m ', end ="")
    else:
      self.updateSuccessCounters(False)
      print('\u001b[31m \u2717 ' + self.CZ + ' = ' + self.DE  + '\033[0m')
      self.repeatToRemember()
    return