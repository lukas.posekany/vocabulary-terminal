from WordsSet import WrdsSetInstance
from commands import commands 
import traceback

class MyException(Exception):
    """Exception for command"""

WrdsSetInstance.loadWordsFile('./words/w1.json')
WrdsSetInstance.printWords()

while(True):
  try:
    w = WrdsSetInstance.wordsList[0]
    wordInput = input(w.CZ + ': ')
    for command in commands:
      if wordInput == command.command:
        command.action()
        raise MyException('command')

    w.checkWord(wordInput)
    WrdsSetInstance.nextWord(w.attempts <= 0)

  except MyException as my: 
    continue

  except Exception:
    traceback.print_exc()
