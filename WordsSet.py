import json
from Word import Word 
from constants import acceptRatio
import types

class WordsSet:
  wordsList = []
  fileName = ''
  def addWord(self, word):
    self.wordsList.append(Word(word))
  
  def loadWordsFile(self, fileName):
    self.wordsList = []
    self.fileName = fileName
    f = open(fileName)
    data = json.loads(f.read())
    for w in data['wordsList']:
      self.addWord(w)
    f.close()

  def restartWordsSetStatistic(self):
    for w in self.wordsList:
      w.restartWordStatistic()

  def writeWords(self):
    printWordsObject = types.SimpleNamespace()
    printWordsObject.wordsList = []
    for w in self.wordsList:
      printWordsObject.wordsList.append(w.simplify())

    wordsString = json.dumps(printWordsObject, default=vars)
    f = open(self.fileName, "w")
    f.write(wordsString)
    f.close()

  def nextWord(self, oldFinished):
    if(oldFinished):
      print('\u001b[32m \t attempts left: ' + str(self.wordsList[0].attempts) + '\t success: ' + str(self.wordsList[0].successRatio) + ' \033[0m')
      self.wordsList[0].restartAttempts()
      self.wordsList = [*self.wordsList[1:len(self.wordsList)], *[self.wordsList[0]]]
    else:
      print('attempts left: \u001b[31m' + str(self.wordsList[0].attempts) + '\t success: ' + str(self.wordsList[0].successRatio) + ' \033[0m')
      self.wordsList = [*self.wordsList[1:3], *[self.wordsList[0]], *self.wordsList[3:len(self.wordsList)]]

  def printWords(self):
    print('\n')
    print(self.fileName)
    print("{:<30} {:<30} {:<5} {:<5}".format('DE', 'CZ', 'att', 'ratio'))
    for w in self.wordsList:
      if w.successRatio < acceptRatio:
        print("{:<30} {:<30} {:<5} \u001b[31m{:<5}\033[0m ".format(w.DE, w.CZ, w.attempts, w.successRatio))
      else:
        print("{:<30} {:<30} {:<5} \033[92m{:<5}\033[0m ".format(w.DE, w.CZ, w.attempts, w.successRatio))
    print('\n')

WrdsSetInstance = WordsSet()